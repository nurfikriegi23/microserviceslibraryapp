﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentWebAPI.Models;

namespace StudentWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly StudentDbContext _studentDbContext;
        public StudentController(StudentDbContext studentDbContext) {
            _studentDbContext = studentDbContext;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Student>> GetStudents()
        {
            return _studentDbContext.Students;
        }

        [HttpGet("{StudentId:int}")]
        public async Task<ActionResult<Student>> GetById(int StudentId)
        {
            var student = await _studentDbContext.Students.FindAsync(StudentId);
            return Ok(student);
        }

        [HttpPost]
        public async Task<ActionResult> Create(Student student)
        {
            await _studentDbContext.Students.AddAsync(student);
            await _studentDbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut]
        public async Task<ActionResult> Update(Student student)
        {
            _studentDbContext.Students.Update(student);
            await _studentDbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("{studentId:int}")]
        public async Task<ActionResult> Delete(int studentId)
        {
            var student = await _studentDbContext.Students.FindAsync(studentId);
            _studentDbContext.Students.Remove(student);
            await _studentDbContext.SaveChangesAsync();
            return Ok();
        }
    }
}
